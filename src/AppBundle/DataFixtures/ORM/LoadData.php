<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use AppBundle\Entity\ExamResult;
use AppBundle\Entity\Exam;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadData
 *
 * @package AppBundle\DataFixtures\ORM
 */
class LoadData implements FixtureInterface
{

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        /** @var User $userAdmin */
        $userAdmin = new User();
        $userAdmin->setUsername('admin');
        $userAdmin->setPlainPassword('admin');
        $userAdmin->setEmail('admin@email.com');
        $userAdmin->setEnabled(true);
        $userAdmin->setRoles(array('ROLE_ADMIN'));
        $dateOfBirth = new \DateTime;
        $dateOfBirth->modify("-18 years");
        $dateOfBirth->modify("-" . rand(1, 365) . " days");
        $userAdmin->setDateOfBirth($dateOfBirth);
        $manager->persist($userAdmin);

        /** @var User $user1 */
        $user1 = new User();
        $user1->setUsername('student1');
        $user1->setPlainPassword('student');
        $user1->setEmail('student1@email.com');
        $user1->setEnabled(true);
        $user1->setRoles(array('ROLE_USER'));
        $user1->setFullName('Frida Lang');
        $dateOfBirth = new \DateTime;
        $dateOfBirth->modify("-18 years");
        $dateOfBirth->modify("-" . rand(1, 365) . " days");
        $user1->setDateOfBirth($dateOfBirth);
        $manager->persist($user1);

        /** @var User $user2 */
        $user2 = new User();
        $user2->setUsername('student2');
        $user2->setPlainPassword('student');
        $user2->setEmail('student2@email.com');
        $user2->setEnabled(true);
        $user2->setRoles(array('ROLE_USER'));
        $user2->setFullName('Mike Oldford');
        $dateOfBirth = new \DateTime;
        $dateOfBirth->modify("-18 years");
        $dateOfBirth->modify("-" . rand(1, 365) . " days");
        $user2->setDateOfBirth($dateOfBirth);
        $manager->persist($user2);

        /** @var User $user3 */
        $user3 = new User();
        $user3->setUsername('student3');
        $user3->setPlainPassword('student');
        $user3->setEmail('student3@email.com');
        $user3->setEnabled(true);
        $user3->setRoles(array('ROLE_USER'));
        $user3->setFullName('John Doe');
        $dateOfBirth = new \DateTime;
        $dateOfBirth->modify("-18 years");
        $dateOfBirth->modify("-" . rand(1, 365) . " days");
        $user3->setDateOfBirth($dateOfBirth);
        $manager->persist($user3);

        /** @var User $user4 */
        $user4 = new User();
        $user4->setUsername('student4');
        $user4->setPlainPassword('student');
        $user4->setEmail('student4@email.com');
        $user4->setEnabled(true);
        $user4->setRoles(array('ROLE_USER'));
        $user4->setFullName('Lisa Walker');
        $dateOfBirth = new \DateTime;
        $dateOfBirth->modify("-18 years");
        $dateOfBirth->modify("-" . rand(1, 365) . " days");
        $user4->setDateOfBirth($dateOfBirth);
        $manager->persist($user4);

        /** @var Exam $exam1 */
        $exam1 = new Exam();
        $exam1->setName("Lorem ipsum dolor sit amet");
        $manager->persist($exam1);
        $exams[] = $exam1;

        /** @var Exam $exam2 */
        $exam2 = new Exam();
        $exam2->setName("In lectus ligula");
        $manager->persist($exam2);
        $exams[] = $exam2;

        /** @var Exam $exam3 */
        $exam3 = new Exam();
        $exam3->setName("Phasellus eleifend");
        $manager->persist($exam3);
        $exams[] = $exam3;

        /** @var Exam $exam4 */
        $exam4 = new Exam();
        $exam4->setName("Aenean vitae urna et");
        $manager->persist($exam4);
        $exams[] = $exam4;

        /** @var Exam $exam5 */
        $exam5 = new Exam();
        $exam5->setName("Cras erat quam");
        $manager->persist($exam5);
        $exams[] = $exam5;

        /** @var Exam $exam6 */
        $exam6 = new Exam();
        $exam6->setName("Integer vitae lectus");
        $manager->persist($exam6);
        $exams[] = $exam6;

        /** @var Exam $exam7 */
        $exam7 = new Exam();
        $exam7->setName("Maecenas ac bibendum est");
        $manager->persist($exam7);
        $exams[] = $exam7;

        /** @var Exam $exam8 */
        $exam8 = new Exam();
        $exam8->setName("Quisque condimentum");
        $manager->persist($exam8);
        $exams[] = $exam8;

        /** @var Exam $exam9 */
        $exam9 = new Exam();
        $exam9->setName("Fusce aliquam mollis vestibulum");
        $manager->persist($exam9);
        $exams[] = $exam9;

        $manager->flush();

        for ($i = 1; $i <= 16; $i++) {
            /** @var Exam[] $exam */
            $today = new \DateTime;
            $today->modify("-" . rand(1, 99) . " days");
            $today->setTime(10, 00, 00);
            $exam[$i] = new ExamResult();
            $exam[$i]->setExam($this->getExamEntity($manager));
            $exam[$i]->setMark(rand(1, 5));
            if ($i % 4 == 0) {
                $exam[$i]->setUser($user4);
            } elseif ($i % 3 == 0) {
                $exam[$i]->setUser($user3);
            } elseif ($i % 2 == 0) {
                $exam[$i]->setUser($user2);
            } elseif ($i % 1 == 0) {
                $exam[$i]->setUser($user1);
            }

            $exam[$i]->setExamDate($today);
            $manager->persist($exam[$i]);
        }

        $manager->flush();
    }

    /**
     * Get exam title.
     *
     * @param ObjectManager $manager
     *
     * @return Exam
     */
    private function getExamEntity(ObjectManager $manager)
    {
        $exams = $manager->getRepository('AppBundle:Exam')->findAll();
        return $exams[rand(0, count($exams) - 1)];
    }
}
