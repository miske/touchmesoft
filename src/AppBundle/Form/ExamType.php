<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Exam;
use AppBundle\Validator\Constraints\ExamNameIsUnique;
use Symfony\Component\Validator\Constraints\NotBlank;

class ExamType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('name', 'text', array(
            'constraints' => array(
              new NotBlank(array(
                'message' => 'Please enter exam title'
              )),
              new ExamNameIsUnique(),
            ),
          ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
          'data_class' => 'AppBundle\Entity\Exam',
          'intention' => 'exam'
        ));
    }
}
