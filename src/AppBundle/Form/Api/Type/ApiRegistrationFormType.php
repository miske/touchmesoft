<?php

namespace AppBundle\Form\Api\Type;

use AppBundle\Validator\Constraints\EmailIsUnique;
use AppBundle\Validator\Constraints\UsernameIsUnique;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;


class ApiRegistrationFormType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
            ->add('email', 'email', array(
                'label' => 'Email address',
                'label_attr' => array(
                    'class' => 'sr-only'
                ),
                'constraints' => array(
                    new NotBlank(array(
                        'message' => 'Please enter email address'
                    )),
                    new Email(array(
                        'message' => 'Please enter a valid email address'
                    )),
                    new EmailIsUnique(),
                ),
                'attr' => array(
                    'placeholder' => 'Email address',
                    'class' => ''
                )))
            ->add('username', 'text', array(
                'label' => 'Username',
                'label_attr' => array(
                    'class' => ''
                ),
                'constraints' => array(
                    new NotBlank(array(
                        'message' => 'Please enter username'
                    )),
                    new UsernameIsUnique(),
                ),
                'attr' => array(
                    'placeholder' => 'Username',
                    'class' => ''
                )))                
            ->add('plainPassword', 'repeated', array(
                'type' => 'password',
                'options' => array('translation_domain' => 'FOSUserBundle'),
                'first_options' => array(
                    'label' => 'Password',
                    'label_attr' => array(
                        'class' => ''
                    ),
                    'constraints' => array(
                        new Length(array(
                            'min' => 6,
                            'minMessage' => "Your password should have at least {{ limit }} characters.",
                        )),
                        new NotBlank(array(
                            'message' => 'Password should not be blank'
                        )),
                    ),
                    'attr' =>array(
                        'placeholder' => 'Password',
                        'class' => ''
                    )),
                'second_options' => array(
                    'label' => 'Confirm password',
                    'label_attr' => array(
                        'class' => ''
                    ),
                    'attr' => array(
                        'placeholder' => 'Confirm Password',
                        'class' => ''
                    )),
                'invalid_message' => 'fos_user.password.mismatch',
            ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        parent::setDefaultOptions($resolver);
        $resolver->setDefaults(array(
            'csrf_protection' => false,
            'intention' => 'user_registration_api',
        ));
    }

    public function getName() {
        return 'user_registration_api';
    }

}

