<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\ExamResult;
use AppBundle\Form\DataTransformer\ExamToStringTransformer;
use Doctrine\Common\Persistence\ObjectManager;

class ExamResultType extends AbstractType
{
    private $manager;

    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('examDate', 'datetime')
          ->add('mark', 'choice', array(
            'placeholder' => 'Choose an mark',
            'choices' => ExamResult::getExamMarks()
          ))
          ->add('user', 'entity', array(
            'class' => 'AppBundle:User',
            'placeholder' => 'Choose an student',
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('u')
                  ->where('u.enabled = :enabled')
                  ->andWhere('u.roles NOT LIKE :roles1')
                  ->setParameters(array('enabled' => TRUE, 'roles1' => '%ROLE_ADMIN%'))
                  ->orderBy('u.fullName', 'ASC');
            },
            'choice_label' => 'fullName',
          ))
          ->add('exam', 'text', array(
              'attr' => array('class' => 'js-exam-autocomplete form-control', 'data-href' => $options['examAutocompleteUrl']),
              // validation message if the data transformer fails
            'invalid_message' => 'Exam name should be at least 3 characters long',
          ));
        $builder->get('exam')
          ->addModelTransformer(new ExamToStringTransformer($this->manager));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
          'data_class' => 'AppBundle\Entity\ExamResult',
          'examAutocompleteUrl'   => null,  
        ));
    }
}
