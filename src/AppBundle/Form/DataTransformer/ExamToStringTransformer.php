<?php
namespace AppBundle\Form\DataTransformer;

use AppBundle\Entity\Exam;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class ExamToStringTransformer implements DataTransformerInterface
{
    private $manager;

    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Transforms an object (exam) to a string (name).
     *
     * @param  Issue|null $exam
     * @return string
     */
    public function transform($exam)
    {
        if (null === $exam) {
            return '';
        }

        return $exam->getName();
    }

    /**
     * Transforms a string (name) to an object (exam).
     *
     * @param  string $examName
     * @return Exam|null
     * @throws TransformationFailedException if object (exam) is not found.
     */
    public function reverseTransform($examName)
    {   
        if (strlen($examName) < 3) {
            throw new TransformationFailedException(sprintf(
                'An exam with name "%s" does not exist!',
                $examName
            ));

        }

        $exam = $this->manager
            ->getRepository('AppBundle:Exam')
            // query for the exam with this name
            ->findOneBy(array('name' => $examName))
        ;

        /*
         * 
         * uncomment this line in case you want only exams from database
        
        if (null === $exam) {
            // causes a validation error
            // this message is not shown to the user
            // see the invalid_message option
            throw new TransformationFailedException(sprintf(
                'An exam with name "%s" does not exist!',
                $examName
            ));
        }

         */
        
        /*
         * in case exam not exist create a new one
         */
        if (null === $exam) {
          $exam = new Exam();
          $exam->setName($examName);
          $this->manager->persist($exam);
          $this->manager->flush();
        }

        return $exam;
    }
}