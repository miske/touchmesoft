<?php

namespace AppBundle\Repository;

use AppBundle\Entity\User;

/**
 * UserRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class UserRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * Find all enabled students
     *
     * @param string|null $orderBy
     * @param string      $order
     *
     * @return array
     */
    public function getOrderableActiveStudents($orderBy = null, $order = 'desc')
    {
        $query = $this->createQueryBuilder('u')
          ->where('u.enabled = :active')
          ->andWhere('u.roles NOT LIKE :roles1');
        switch ($orderBy) {
            case 'name':
                $query->orderBy('u.fullName', $order);
                break;
            case 'birth':
                $query->orderBy('u.dateOfBirth', $order);
                break;
            default:
                $query->orderBy('u.id', $order);
        }

        $query->setParameters(array('active' => TRUE, 'roles1' => '%ROLE_ADMIN%'));
        $data = $query->getQuery()->getResult();

        return $data;
    }
}
