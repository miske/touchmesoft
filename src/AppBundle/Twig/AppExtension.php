<?php
namespace AppBundle\Twig;

class AppExtension extends \Twig_Extension
{
    public function getFunctions()
    {
        return array(
          new \Twig_SimpleFunction('getSortedOrderClass', array($this, 'getSortedOrderClass')),
        );
    }

    /**
     * @param $orderBy
     * @param $order
     * @param $thTitle
     *
     * @return string
     */
    public function getSortedOrderClass($orderBy, $order, $thTitle)
    {
        $class = "";
        if ($orderBy && $orderBy == $thTitle) {
            if ($order && $order == 'asc') {
                $class = "fa-sort-amount-asc";
            } else {
                $class = "fa-sort-amount-desc";
            }
        }
        return $class;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_extension';
    }
}