<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ExamResult
 *
 * @ORM\Table(name="exam_result")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ExamResultRepository")
 */
class ExamResult
{
    CONST MARK_1 = 1;
    CONST MARK_2 = 2;
    CONST MARK_3 = 3;
    CONST MARK_4 = 4;
    CONST MARK_5 = 5;
  
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="examDate", type="datetime")
     * @Assert\NotBlank(
     *    message = "Please enter exam date"
     * )
     * @Assert\LessThan("today")
     */
    private $examDate;

    /**
     * @var int
     *
     * @ORM\Column(name="mark", type="integer")
     * @Assert\NotBlank(
     *    message = "Please enter exam mark"
     * )
     */
    private $mark;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="examResults")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Exam", inversedBy="examResults", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="exam_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $exam;
    
    /**
     * @return array
     */        
    public static function getExamMarks()
    {
      return array(
        self::MARK_5 => 'A',
        self::MARK_4 => 'B',
        self::MARK_3 => 'C',
        self::MARK_2 => 'D',
        self::MARK_1 => 'E',
      );
    }
        
    /**
     * @return mixed 
     */    
    public function __toString()
    {   
      return $this->user . ": " . $this->exam;
    }
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set examDate
     *
     * @param \DateTime $examDate
     *
     * @return ExamResult
     */
    public function setExamDate($examDate)
    {
        $this->examDate = $examDate;

        return $this;
    }

    /**
     * Get examDate
     *
     * @return \DateTime
     */
    public function getExamDate()
    {
        return $this->examDate;
    }

    /**
     * Set mark
     *
     * @param integer $mark
     *
     * @return ExamResult
     */
    public function setMark($mark)
    {
        $this->mark = $mark;

        return $this;
    }

    /**
     * Get mark
     *
     * @return int
     */
    public function getMark()
    {
        return $this->mark;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return ExamResult
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set exam
     *
     * @param \AppBundle\Entity\Exam $exam
     *
     * @return ExamResult
     */
    public function setExam(\AppBundle\Entity\Exam $exam = null)
    {
        $this->exam = $exam;

        return $this;
    }

    /**
     * Get exam
     *
     * @return \AppBundle\Entity\Exam
     */
    public function getExam()
    {
        return $this->exam;
    }
    
    /**
     * Get exam mark
     *
     * @return string
     */
    public function getExamMark()
    {
        $examMarks = $this->getExamMarks();
        return $examMarks[$this->mark];
    }

}
