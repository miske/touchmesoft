<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Exam
 *
 * @ORM\Table(name="exam")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ExamRepository")
 */
class Exam
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank(
     *    message = "Please enter exam name"
     * )
     * @Assert\Length(
     *      min = 3,
     *      minMessage = "Exam name should be at least {{ limit }} characters long",
     * )
     */
    private $name;
     
    /**
     * @ORM\OneToMany(
     *      targetEntity="ExamResult",
     *      mappedBy="exam",
     *      cascade={"persist", "remove"},
     *      orphanRemoval=true
     * )
     * @ORM\OrderBy({"examDate" = "DESC"})
     */
    private $examResults;
    
    /**
     * Default constructor
     */
    public function __construct()
    {
        $this->examResults = new ArrayCollection();
    }    
    
    /**
     * @return mixed 
     */    
    public function __toString()
    {   
      return $this->name;
    }
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Exam
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Add examResult
     *
     * @param \AppBundle\Entity\ExamResult $examResult
     *
     * @return Exam
     */
    public function addExamResult(\AppBundle\Entity\ExamResult $examResult)
    {
        $this->examResults[] = $examResult;

        return $this;
    }

    /**
     * Remove examResult
     *
     * @param \AppBundle\Entity\ExamResult $examResult
     */
    public function removeExamResult(\AppBundle\Entity\ExamResult $examResult)
    {
        $this->examResults->removeElement($examResult);
    }

    /**
     * Get exams
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getExamResults()
    {
        return $this->examResults;
    }
}

