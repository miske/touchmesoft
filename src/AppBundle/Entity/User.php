<?php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Util\IDEncrypt;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $apiToken;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     * @Assert\NotBlank(
     *    message = "Please enter student name"
     * )
     * @Assert\Length(
     *      min = 3,
     *      minMessage = "Student name should be at least {{ limit }} characters long",
     * )
     */
    private $fullName;
      
    /** 
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\NotBlank(
     *    message = "Please enter student birthday"
     * )
     * @Assert\LessThan("-18 years")
     */
    private $dateOfBirth;
    
    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;    
    
    /** 
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;
     
    /**
     * @ORM\OneToMany(
     *      targetEntity="ExamResult",
     *      mappedBy="user",
     *      orphanRemoval=true
     * )
     * @ORM\OrderBy({"examDate" = "DESC"})
     */
    private $examResults;
    
    /**
     * Default constructor
     */
    public function __construct()
    {
        parent::__construct();  
        $this->examResults = new ArrayCollection();
    }
    
    /**
     * @return mixed 
     */    
    public function __toString()
    {   
      return $this->fullName;
    }
    
    /**  
     * @ORM\PrePersist
     */
    public function doStuffOnPrePersist()
    {
      if(!$this->getCreatedAt())
      {
        $this->createdAt = new \DateTime();
      }
    }
    
    /**
     * @ORM\PreUpdate
     */
    public function doStuffOnPreUpdate()
    {
      $this->updatedAt = new \DateTime();
    }

    
    /**
     * @return mixed
     */
    public function getApiToken()
    {
        return $this->apiToken;
    }

    /**
     * @param mixed $apiToken
     * @return User
     */
    public function setApiToken()
    {
        $this->apiToken = IDEncrypt::encrypt(time());
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * @param mixed $fullName
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;
        return $this;
    }

    /**
     * Set dateOfBirth
     *
     * @param \DateTime $dateOfBirth
     * @return User
     */
    public function setDateOfBirth($dateOfBirth)
    {
        $this->dateOfBirth = $dateOfBirth;

        return $this;
    }

    /**
     * Get dateOfBirth
     *
     * @return \DateTime 
     */
    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }
    
    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    
    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return User
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add examResult
     *
     * @param \AppBundle\Entity\ExamResult $examResult
     *
     * @return User
     */
    public function addExamResult(\AppBundle\Entity\ExamResult $examResult)
    {
        $this->examResults[] = $examResult;

        return $this;
    }

    /**
     * Remove examResult
     *
     * @param \AppBundle\Entity\ExamResult $examResult
     */
    public function removeExamResult(\AppBundle\Entity\ExamResult $examResult)
    {
        $this->examResults->removeElement($examResult);
    }

    /**
     * Get exams
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getExamResults()
    {
        return $this->examResults;
    }
}
