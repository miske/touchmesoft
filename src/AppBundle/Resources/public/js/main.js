$(document).ready(function () {

    $(document).on('click', '.js-show-modal', function (e) {
        e.preventDefault();
        var obj = $(this);
        var title = obj.data('title');
        if (obj.data('url')) {
            $.get($(this).data('url'), function (data) {
                $('.js-modal-body').html(data.html);
                if (data.title) title = data.title;
                $('.js-modal-title').html(title);
                $('#js-main-modal').modal();
                initAutocompleteExams();
                if (data.examId) {
                    setDataToExamAutocomplete(data.examId, data.examName);
                }
            });
        }
    });

    $(document).on('submit', '.js-ajax-form', function (e) {
        e.preventDefault();
        var obj = $(this);
        var target = $(this).data('target');
        $.post($(this).attr('action'), $(this).serialize(), function (data) {
            if (data.success == true) {
                $(target).html(data.html);
                if ($('#js-main-modal').is(':visible')) {
                    $('#js-main-modal').modal('hide');
                }
            } else {
                $('.js-modal-body').html(data.html);
                initAutocompleteExams();
                if (data.examId) {
                    setDataToExamAutocomplete(data.examId, data.examName);
                }
            }
            if (data.flashMessage) {
                Flash.show(data.flashMessage);
            }
        });
    });

    $(document).on("click", ".js-delete", function (e) {
        e.preventDefault();
        var title = $(this).data('title') ? $(this).data('title') : 'Are you sure?';
        var url = $(this).data('href');
        var target = $(this).data('target');
        bootbox.confirm(title, function (result) {
            if (result === true) {
                $.post(url, function (data) {
                    if (data.success == true) {
                        $(target).html(data.html);
                    }
                    if (data.flashMessage) {
                        Flash.show(data.flashMessage);
                    }
                });
            }
        });
    });


    $(document).on('click', '.js-ajax-order', function (e) {
        e.preventDefault();
        var obj = $(this);
        var target = $(this).data('target');
        $.post($(this).attr('href'), function (data) {
            if (data.success == true) {
                $(target).html(data.html);
            }
        });
    });
    
    $(document).on('click', '.js-user-exam', function (e) {
        e.preventDefault();
        var obj = $(this);
        $.get(obj.attr('href'), function (data) {
          $(".js-user-exam-holder").html(data.html);
          if ($(".js-user-exam-holder").is(':visible') !== true) {
              $(".js-user-exam-holder").show();
          } 
          $('html, body').animate({
              scrollTop: $(".js-user-exam-holder").offset().top
          }, 1000);
        });
        
    });
});

function initAutocompleteExams() {
    if ($('input.js-exam-autocomplete').length > 0) {

        $("input.js-exam-autocomplete").select2({
            minimumInputLength: 2,
            multiple: false,
            ajax: {
                url: $('input.js-exam-autocomplete').data('href'),
                dataType: 'json',
                data: function (term) {
                    return {
                        term: term, // search term
                        page_limit: 10
                    };
                },
                results: function (data) {
                    return {results: data};
                }
            },
            id: function (object) {
                return object.text;
            },
            //Allow manually entered text in drop down.
            createSearchChoice: function (term, data) {
                if ($(data).filter(function () {
                        return this.text.localeCompare(term) === 0;
                    }).length === 0) {
                    return {id: term, text: term};
                }
            },
        });
    }
}

function setDataToExamAutocomplete(id, key) {
    $("input.js-exam-autocomplete").select2('data', {id: id, text: key});
}