<?php

namespace AppBundle\Handler;

use AppBundle\Entity\User;
use AppBundle\Entity\IndemnityInsuranceFile;

class UserHelper
{
    private $doctrine;

    public function __construct($doctrine)
    {
        $this->doctrine = $doctrine;
    }

    /**
     * @param $fullName
     * @param $user
     *
     * @return int|mixed|string
     */
    public function generateUsernameFromName($fullName, &$user)
    {
        $username = strtolower($fullName);
        $username = preg_replace('/[^[:alnum:]]/', ' ', $username);
        $username = trim(preg_replace('/[[:space:]]+/', '-', $username));
        if ($username != '' && !$this->isUsernameTaken($username)) {
            $user->setUsername($username);

            return $username;
        } else {
            $attempt = 1;
            $username != '' ? $newUsername = $username . '-' . $attempt : $newUsername = $attempt;
            while ($this->isUsernameTaken($newUsername)) {
                $attempt++;
                $username != '' ? $newUsername = $username . '-' . $attempt : $newUsername = $attempt;
            }

            $user->setUsername($newUsername);

            return $newUsername;
        }

    }

    /**
     * Generate email from FullName from the given user.
     *
     * @param $fullName
     * @param $user
     *
     * @return mixed|string
     */
    public function generateEmailFromName($fullName, &$user)
    {
        $email = strtolower($fullName);
        $email = preg_replace('/[^[:alnum:]]/', ' ', $email);
        $email = trim(preg_replace('/[[:space:]]+/', '-', $email)) . "@email.com";
        if ($email != '' && !$this->isEmailTaken($email)) {
            $user->setEmail($email);

            return $email;
        } else {
            $attempt = 1 . "@email.com";
            $email != '' ? $newEmail = $email . '-' . $attempt : $newEmail = $attempt;
            while ($this->isEmailTaken($newEmail)) {
                $attempt++;
                $email != '' ? $newEmail = $email . '-' . $attempt : $newEmail = $attempt;
            }

            $user->setEmail($newEmail);

            return $newEmail;
        }
    }

    /**
     * Generate email from FullName from the given user.
     *
     * @param $fullName
     * @param $user
     *
     * @return mixed
     */
    public function generateUserDataFromName($fullName, &$user)
    {
        $user->setUsername($this->generateUsernameFromName($fullName, $user));
        $user->setEmail($this->generateEmailFromName($fullName, $user));
        $user->setPlainPassword('student');
        $user->setEnabled(true);
        $user->setRoles(array('ROLE_USER'));
        return $user;
    }

    /**
     * Checks if username is already taken by any user
     *
     * @param string $username
     *
     * @return boolean
     */
    private function isUsernameTaken($username)
    {
        $em = $this->doctrine->getManager();
        $user = $em->getRepository('AppBundle:User')->findBy(array('username' => $username));
        if ($user && count($user))
            return true;
        else
            return false;
    }


    /**
     * Checks if email is already taken by any user
     *
     * @param string $email
     *
     * @return boolean
     */
    public function isEmailTaken($email)
    {
        $em = $this->doctrine->getManager();
        $user = $em->getRepository('AppBundle:User')->findBy(array('email' => $email));
        if ($user && count($user))
            return true;
        else
            return false;
    }
}