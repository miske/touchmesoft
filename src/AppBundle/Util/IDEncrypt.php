<?php

namespace AppBundle\Util;

use AppBundle\Util\Encryption;

class IDEncrypt {

  private static $Encr;

  private static function getEncr() {
    if (!self::$Encr instanceof Encryption) {
      self::$Encr = new Encryption();
    }
    return self::$Encr;
  }

  /**
   * @param  raw int
   * @return encrypted string 
   */
  public static function encrypt($id) {
    return self::getEncr()->encode($id);
  }

  /**
   * @param  raw int
   * @return decrypted string 
   */
  public static function decrypt($encrypted) {
    return self::getEncr()->decode($encrypted);
  }

}

