<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Doctrine\ORM\EntityManager;

class EmailIsUniqueValidator extends ConstraintValidator
{
    private $entityManager;

    /**
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param mixed      $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        $repository = $this->entityManager->getRepository('AppBundle:User');
        $entry = $repository->findOneBy(array('email' => $value));

        if ($entry) {
            $this->context->addViolation($constraint->message, array('%string%' => $value));
        }

    }
}

?>
