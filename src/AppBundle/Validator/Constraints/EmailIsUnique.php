<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class EmailIsUnique extends Constraint
{
    public $message = 'The user with this email address has already registered.';
    
    public function ValidatedBy()
    {
        return 'is_unique_email_validator';
    }
}


