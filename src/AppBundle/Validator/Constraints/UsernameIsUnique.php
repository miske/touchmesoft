<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class UsernameIsUnique extends Constraint
{
    public $message = 'The user with this username has already registered.';
    
    public function ValidatedBy()
    {
        return 'is_unique_username_validator';
    }
}


