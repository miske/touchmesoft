<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ExamNameIsUnique extends Constraint
{
    public $message = 'The exam with this name has already exist.';
    
    public function ValidatedBy()
    {
        return 'is_unique_examname_validator';
    }
}


