<?php

namespace AppBundle\Event\Api;

class AppEvents
{
    const USER_REGISTRATED = "app_registration.registrated";
    const USER_CONFIRMED = "app_core_registration.confirmed";
} 

