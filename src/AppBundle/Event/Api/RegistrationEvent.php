<?php

namespace AppBundle\Event\Api;

use Symfony\Component\EventDispatcher\Event;


class RegistrationEvent extends Event
{
    protected $user;

    /**
     * @param $user
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }
} 
