<?php

namespace AppBundle\Controller;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use AppBundle\Controller\AdminBaseController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * Admin controller.
 *
 * @Route("/admin/user")
 */
class AdminUserController extends AdminBaseController
{

    /**
     * Lists all User entities.
     *
     * @Route("/", name="admin_user_index")
     * @Method({"GET", "POST"})
     * @param Request $request
     *
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $user = $this->getUserCheck();

        $em = $this->getDoctrine()->getManager();
        $orderBy = $request->query->get('orderby', NULL);
        $order = $request->query->get('order', NULL);
        $users = $em->getRepository('AppBundle:User')->getOrderableActiveStudents($orderBy, $order);

        if ($request->isXmlHttpRequest()) {
            $html = $this->renderView('user/user_table.html.twig', array(
                'users' => $users,
                'orderBy' => $orderBy,
                'order' => $order
            ));
            $success = true;
            return new JsonResponse(array(
                    'html' => $html,
                    'success' => $success
                )
            );
        } else {
            return $this->render('user/index.html.twig', array(
                'users' => $users,
                'orderBy' => $orderBy,
                'order' => $order
            ));
        }
    }

    /**
     * Creates a new User entity.
     *
     * @Route("/new", name="user_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     *
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm('AppBundle\Form\UserType', $user);
        $form->handleRequest($request);

        $orderBy = $request->query->get('orderby', NULL);
        $order = $request->query->get('order', NULL);
        $submitPath = $this->generateUrl('user_new', array('orderby' => $orderBy, 'order' => $order));
        $isAjax = $request->isXmlHttpRequest();

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $userHelper = $this->container->get('app.user_helper');
                $userHelper->generateUserDataFromName($user->getFullName(), $user);
                $em->persist($user);
                $em->flush();

                if (!$isAjax) {
                    return $this->redirectToRoute('user_index');
                }

                $users = $em->getRepository('AppBundle:User')->getOrderableActiveStudents($orderBy, $order);
                $html = $this->renderView('user/user_table.html.twig', array(
                    'users' => $users,
                    'orderBy' => $orderBy,
                    'order' => $order,
                    'isAjax' => $isAjax
                ));
                $success = true;
                $flashMessage = 'You have succesfully created student.';
            } else {
                if (!$isAjax) {
                    return $this->render('user/new.html.twig', array(
                        'user' => $user,
                        'form' => $form->createView(),
                        'submitPath' => $submitPath,
                        'orderBy' => $orderBy,
                        'order' => $order,
                        'isAjax' => $isAjax
                    ));
                }

                $html = $this->renderView('user/new_form.html.twig', array(
                    'user' => $user,
                    'form' => $form->createView(),
                    'submitPath' => $submitPath,
                    'isAjax' => $isAjax
                ));
                $success = false;
                $flashMessage = 'You have some errors in form.';
            }
            return new JsonResponse(array(
                    'html' => $html,
                    'success' => $success,
                    'flashMessage' => $flashMessage
                )
            );
        }

        if ($isAjax) {
            return new JsonResponse(array(
                    'html' => $this->renderView('user/new_form.html.twig', array(
                        'user' => $user,
                        'form' => $form->createView(),
                        'submitPath' => $submitPath,
                        'isAjax' => $isAjax
                    )),
                    'success' => true
                )
            );
        } else {
            return $this->render('user/new.html.twig', array(
                'user' => $user,
                'form' => $form->createView(),
                'submitPath' => $submitPath,
                'orderBy' => $orderBy,
                'order' => $order,
                'isAjax' => $isAjax
            ));
        }
    }

    /**
     * Finds and displays a User entity.
     *
     * @Route("/{username}", name="user_show")
     * @ParamConverter("user", class="AppBundle:User", options={"mapping": {"username": "username"}})
     * @param User $user
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(User $user)
    {
        $deleteForm = $this->createDeleteForm($user);

        return $this->render('user/show.html.twig', array(
            'user' => $user,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/{username}/edit", name="user_edit")
     * @ParamConverter("user", class="AppBundle:User", options={"mapping": {"username": "username"}})
     * @param Request $request
     * @param User $user
     *
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, User $user)
    {
        $deleteForm = $this->createDeleteForm($user);
        $editForm = $this->createForm('AppBundle\Form\UserType', $user);
        $editForm->handleRequest($request);

        $orderBy = $request->query->get('orderby', NULL);
        $order = $request->query->get('order', NULL);
        $submitPath = $this->generateUrl('user_edit', array('username' => $user->getUsername(), 'orderby' => $orderBy, 'order' => $order));
        $isAjax = $request->isXmlHttpRequest();

        if ($editForm->isSubmitted()) {
            if ($editForm->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

                if (!$isAjax) {
                    return $this->redirectToRoute('user_index');
                }

                $users = $em->getRepository('AppBundle:User')->getOrderableActiveStudents($orderBy, $order);
                $html = $this->renderView('user/user_table.html.twig', array(
                    'users' => $users,
                    'orderBy' => $orderBy,
                    'order' => $order
                ));
                $success = true;
                $flashMessage = 'You have succesfully updated student.';
            } else {
                if (!$isAjax) {
                    return $this->render('user/edit.html.twig', array(
                        'user' => $user,
                        'form' => $editForm->createView(),
                        'delete_form' => $deleteForm->createView(),
                        'submitPath' => $submitPath,
                        'isAjax' => $isAjax
                    ));
                }

                $html = $this->renderView('user/new_form.html.twig', array(
                    'user' => $user,
                    'form' => $editForm->createView(),
                    'submitPath' => $submitPath,
                    'isAjax' => $isAjax
                ));
                $success = false;
                $flashMessage = 'You have some errors in form.';
            }
            return new JsonResponse(array(
                    'html' => $html,
                    'success' => $success,
                    'flashMessage' => $flashMessage
                )
            );
        }

        if ($isAjax) {
            return new JsonResponse(array(
                    'html' => $this->renderView('user/new_form.html.twig', array(
                        'user' => $user,
                        'form' => $editForm->createView(),
                        'submitPath' => $submitPath,
                        'isAjax' => $isAjax
                    )),
                    'success' => true
                )
            );
        } else {
            return $this->render('user/edit.html.twig', array(
                'user' => $user,
                'form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
                'submitPath' => $submitPath,
                'isAjax' => $isAjax
            ));
        }
    }

    /**
     * Deletes a User entity.
     *
     * @Route("/delete/{username}", name="user_delete")
     * @ParamConverter("user", class="AppBundle:User", options={"mapping": {"username": "username"}})
     * @param Request $request
     * @param User $user
     *
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, User $user)
    {
        if ($request->isXmlHttpRequest()) {
            $orderBy = $request->query->get('orderby', NULL);
            $order = $request->query->get('order', NULL);

            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();

            $users = $em->getRepository('AppBundle:User')->getOrderableActiveStudents($orderBy, $order);
            $html = $this->renderView('user/user_table.html.twig', array(
                'users' => $users,
                'orderBy' => $orderBy,
                'order' => $order
            ));
            $success = true;
            $flashMessage = 'You have succesfully deleted student.';
            return new JsonResponse(array(
                    'html' => $html,
                    'success' => $success,
                    'flashMessage' => $flashMessage
                )
            );

        } else {
            $form = $this->createDeleteForm($user);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->remove($user);
                $em->flush();
            }

            return $this->redirectToRoute('user_index');
        }
    }

    /**
     * Creates a form to delete a User entity.
     *
     * @param User $user The User entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(User $user)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('user_delete', array('username' => $user->getUsername())))
            ->setMethod('DELETE')
            ->getForm();
    }

}