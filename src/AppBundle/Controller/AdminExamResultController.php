<?php

namespace AppBundle\Controller;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use AppBundle\Controller\AdminBaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\ExamResult;
use AppBundle\Form\ExamResultType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * ExamResult controller.
 *
 * @Route("/admin/exam-result")
 */
class AdminExamResultController extends AdminBaseController
{
    /**
     * Lists all Exam result entities.
     *
     * @Route("/", name="admin_exam_result_index")
     * @Method({"GET", "POST"})
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $user = $this->getUserCheck();

        $em = $this->getDoctrine()->getManager();
        $orderBy = $request->query->get('orderby', NULL);
        $order = $request->query->get('order', NULL);
        $examResults = $em->getRepository('AppBundle:ExamResult')->getOrderableExamResults($orderBy, $order);

        if ($request->isXmlHttpRequest()) {
            // Do something...
        } else {
            return $this->render('exam_result/index.html.twig', array(
              'exams' => $examResults,
              'orderBy' => $orderBy,
              'order' => $order
            ));
        }
    }

    /**
     * Creates a new Exam entity.
     *
     * @Route("/new", name="exam_result_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     *
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $manager = $this->getDoctrine()->getManager();
        $examAutocompleteUrl = $this->generateUrl('autocomplete_exam');
        $examResult = new ExamResult();
        $form = $this->createForm(new ExamResultType($manager), $examResult, array('examAutocompleteUrl' => $examAutocompleteUrl));
        $form->handleRequest($request);

        $orderBy = $request->query->get('orderby', NULL);
        $order = $request->query->get('order', NULL);
        $submitPath = $this->generateUrl('exam_result_new', array('orderby' => $orderBy, 'order' => $order));
        $isAjax = $request->isXmlHttpRequest();

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($examResult);
                $em->flush();

                if (!$isAjax) {
                    return $this->redirectToRoute('exam_index');
                }

                $examResults = $em->getRepository('AppBundle:ExamResult')->getOrderableExamResults($orderBy, $order);
                $html = $this->renderView('exam_result/exam_table.html.twig', array(
                  'exams' => $examResults,
                  'orderBy' => $orderBy,
                  'order' => $order,
                  'isAjax' => $isAjax
                ));
                $success = true;
                $flashMessage = 'You have succesfully created exam result.';
            } else {
                if (!$isAjax) {
                    return $this->render('exam_result/new.html.twig', array(
                      'exam' => $examResult,
                      'form' => $form->createView(),
                      'submitPath' => $submitPath,
                      'orderBy' => $orderBy,
                      'order' => $order,
                      'isAjax' => $isAjax
                    ));
                }

                $html = $this->renderView('exam_result/new_form.html.twig', array(
                  'exam' => $examResult,
                  'form' => $form->createView(),
                  'submitPath' => $submitPath,
                  'isAjax' => $isAjax
                ));
                $success = false;
                $flashMessage = 'You have some errors in form.';
            }
            return new JsonResponse(array(
                'html' => $html,
                'success' => $success,
                'flashMessage' => $flashMessage
              )
            );
        }

        if ($isAjax) {
            return new JsonResponse(array(
                'html' => $this->renderView('exam_result/new_form.html.twig', array(
                  'exam' => $examResult,
                  'form' => $form->createView(),
                  'submitPath' => $submitPath,
                  'orderBy' => $orderBy,
                  'order' => $order,
                  'isAjax' => $isAjax
                )))
            );
        } else {
            return $this->render('exam_result/new.html.twig', array(
              'exam' => $examResult,
              'form' => $form->createView(),
              'submitPath' => $submitPath,
              'orderBy' => $orderBy,
              'order' => $order,
              'isAjax' => $isAjax
            ));
        }


    }

    /**
     * Finds and displays a Exam entity.
     *
     * @Route("/show/{id}", name="exam_result_show")
     * @ParamConverter("examResult", class="AppBundle:ExamResult", options={"mapping": {"id": "id"}})
     * @param ExamResult $examResult
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(ExamResult $examResult)
    {
        $deleteForm = $this->createDeleteForm($examResult);

        return $this->render('exam_result/show.html.twig', array(
          'exam' => $examResult,
          'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Exam entity.
     *
     * @Route("/{id}/edit", name="exam_result_edit")
     * @ParamConverter("examResult", class="AppBundle:ExamResult", options={"mapping": {"id": "id"}})
     * @param Request    $request
     * @param ExamResult $examResult
     *
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, ExamResult $examResult)
    {
        $manager = $this->getDoctrine()->getManager();
        $deleteForm = $this->createDeleteForm($examResult);
        $examAutocompleteUrl = $this->generateUrl('autocomplete_exam');
        $editForm = $this->createForm(new ExamResultType($manager), $examResult, array('examAutocompleteUrl' => $examAutocompleteUrl));
        $editForm->handleRequest($request);

        $orderBy = $request->query->get('orderby', NULL);
        $order = $request->query->get('order', NULL);
        $submitPath = $this->generateUrl('exam_result_edit', array('id' => $examResult->getId(), 'orderby' => $orderBy, 'order' => $order));
        $isAjax = $request->isXmlHttpRequest();

        if ($editForm->isSubmitted()) {
            if ($editForm->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($examResult);
                $em->flush();

                if (!$isAjax) {
                    return $this->redirectToRoute('exam_index');
                }

                $examResults = $em->getRepository('AppBundle:ExamResult')->getOrderableExamResults($orderBy, $order);
                $html = $this->renderView('exam_result/exam_table.html.twig', array(
                  'exams' => $examResults,
                  'orderBy' => $orderBy,
                  'order' => $order,
                  'isAjax' => $isAjax
                ));
                $success = true;
                $flashMessage = 'You have succesfully updated exam result.';
            } else {
                if (!$isAjax) {
                    return $this->render('exam_result/edit.html.twig', array(
                      'exam' => $examResult,
                      'form' => $editForm->createView(),
                      'delete_form' => $deleteForm->createView(),
                      'submitPath' => $submitPath,
                      'orderBy' => $orderBy,
                      'order' => $order,
                      'isAjax' => $isAjax
                    ));
                }

                $html = $this->renderView('exam_result/new_form.html.twig', array(
                  'exam' => $examResult,
                  'form' => $editForm->createView(),
                  'submitPath' => $submitPath,
                  'isAjax' => $isAjax
                ));
                $success = false;
                $flashMessage = 'You have some errors in form.';
            }
            return new JsonResponse(array(
                'html' => $html,
                'success' => $success,
                'flashMessage' => $flashMessage,
                'examId' => $examResult->getExam() ? $examResult->getExam()->getId() : '',
                'examName' => $examResult->getExam() ? $examResult->getExam()->getName() : ''
              )
            );
        }

        if ($isAjax) {
            return new JsonResponse(array(
                'html' => $this->renderView('exam_result/new_form.html.twig', array(
                  'exam' => $examResult,
                  'form' => $editForm->createView(),
                  'submitPath' => $submitPath,
                  'isAjax' => $isAjax
                )),
                'success' => true,
                'examId' => $examResult->getExam() ? $examResult->getExam()->getId() : '',
                'examName' => $examResult->getExam() ? $examResult->getExam()->getName() : ''
              )
            );
        } else {
            return $this->render('exam_result/edit.html.twig', array(
              'examResult' => $examResult,
              'form' => $editForm->createView(),
              'delete_form' => $deleteForm->createView(),
              'submitPath' => $submitPath,
              'isAjax' => $isAjax
            ));
        }
    }

    /**
     * Deletes a Exam entity.
     *
     * @Route("/delete/{id}", name="exam_result_delete")
     * @ParamConverter("examResult", class="AppBundle:ExamResult", options={"mapping": {"id": "id"}})
     * @param Request    $request
     * @param ExamResult $examResult
     *
     * @return JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, ExamResult $examResult)
    {
        if ($request->isXmlHttpRequest()) {
            $orderBy = $request->query->get('orderby', NULL);
            $order = $request->query->get('order', NULL);

            $em = $this->getDoctrine()->getManager();
            $em->remove($examResult);
            $em->flush();

            $examResults = $em->getRepository('AppBundle:ExamResult')->getOrderableExamResults($orderBy, $order);
            $html = $this->renderView('exam_result/exam_table.html.twig', array(
              'exams' => $examResults,
              'orderBy' => $orderBy,
              'order' => $order,
            ));
            $success = true;
            $flashMessage = 'You have succesfully deleted exam.';
            return new JsonResponse(array(
                'html' => $html,
                'success' => $success,
                'flashMessage' => $flashMessage
              )
            );
        } else {
            $form = $this->createDeleteForm($examResult);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->remove($examResult);
                $em->flush();
            }

            return $this->redirectToRoute('exam_index');
        }
    }

    /**
     * Creates a form to delete a Exam entity.
     *
     * @param ExamResult $examResult The Exam entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ExamResult $examResult)
    {
        return $this->createFormBuilder()
          ->setAction($this->generateUrl('exam_result_delete', array('id' => $examResult->getId())))
          ->setMethod('DELETE')
          ->getForm();
    }

    /**
     * Finds and display Exam name
     *
     * @Route("/autocomplete-exam", name="autocomplete_exam")
     * @param Request $request
     *
     * @return JsonResponse
     * @throws AccessDeniedHttpException if is not Ajax request
     */
    public function autocompleteExamAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new AccessDeniedHttpException();
        }
        $em = $this->getDoctrine()->getManager();
        $examRepo = $em->getRepository('AppBundle:Exam');
        $term = trim($request->get('term', ''));

        $res = array();
        if ($term != '') {

            $exams = $examRepo->getExamsByKeyword($term);

            foreach ($exams as $exam) {
                $res[] = array(
                  "id" => $exam->getName(),
                  "text" => $exam->getName(),
                );
            }
        }

        return new JsonResponse(
          $res
        );
    }

}
