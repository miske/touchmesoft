<?php

namespace AppBundle\Controller\Api;

use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\View\View;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use AppBundle\Controller\Api\ApiBaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use AppBundle\Util\IDEncrypt;
use AppBundle\Entity\User;
use AppBundle\Form\Api\Type\ApiRegistrationFormType;
use AppBundle\Event\Api\RegistrationEvent;
use AppBundle\Event\Api\AppEvents;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class ApiController extends ApiBaseController
{

    /**
     * Login user
     *
     * @ApiDoc(
     *   resource = true,
     *   section = "Users",
     *   statusCodes = {
     *     201 = "Returned when a new resource is created",
     *     204 = "Returned when successful",
     *     400 = "Returned when the form has errors",
     *   },
     *   requirements={
     *      {
     *          "name"="email",
     *          "dataType"="string",
     *          "requirement"="\d+",
     *          "description"="User email, required"
     *      },
     *      {
     *          "name"="password",
     *          "dataType"="string",
     *          "requirement"="\d+",
     *          "description"="User password, required"
     *      }
     *   }
     * )
     * @param Request $request
     *
     * @return View
     */
    public function postLoginAction(Request $request)
    {

        $email = $request->get('email');
        $password = $request->get('password');

        $userManager = $this->container->get('fos_user.user_manager');

        $factory = $this->container->get('security.encoder_factory');

        $user = $userManager->findUserByEmail($email);

        if (!$user) {

            $view = View::create()
              ->setStatusCode(Codes::HTTP_BAD_REQUEST)
              ->setData(array('message' => 'Invalid email or password'));

            return $this->get('fos_rest.view_handler')->handle($view);
        }

        $encoder = $factory->getEncoder($user);

        if (!$encoder->isPasswordValid($user->getPassword(), $password, $user->getSalt())) {

            $view = View::create()
              ->setStatusCode(Codes::HTTP_BAD_REQUEST)
              ->setData(array('message' => 'Invalid email or password'));

            return $this->get('fos_rest.view_handler')->handle($view);
        }

        $this->container->get('fos_user.security.login_manager')->loginUser(
          $this->container->getParameter('fos_user.firewall_name'), $user
        );

        $userManager = $this->container->get('fos_user.user_manager');
        if (!$user->getApiToken()) {
            $user->setApiToken();
        }
        $userManager->updateUser($user);

        if (!$user->isEnabled()) {
            $view = View::create()
              ->setStatusCode(Codes::HTTP_BAD_REQUEST)
              ->setData(array('message' => 'If you want to login you need to confirm your account. Please check your email.'));

            return $this->get('fos_rest.view_handler')->handle($view);
        } else {
            $view = View::create()
              ->setStatusCode(Codes::HTTP_OK)
              ->setData($this->getUserParams($user));

            return $this->get('fos_rest.view_handler')->handle($view);
        }
    }

    /**
     * Logout user
     *
     * @ApiDoc(
     *   resource = true,
     *   section = "Users",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors",
     *   }
     * )
     *
     * @param Request $request
     *
     * @return View
     */
    public function logoutAction(Request $request)
    {
        $this->get('security.context')->setToken(null);
        $request->getSession()->invalidate();

        $view = View::create()
          ->setStatusCode(Codes::HTTP_OK)
          ->setData(array('message' => 'Successfully logout'));

        return $this->get('fos_rest.view_handler')->handle($view);
    }


    /**
     * Register user
     *
     * @ApiDoc(
     *   resource = true,
     *   section = "Users",
     *   statusCodes = {
     *     201 = "Returned when a new resource is created",
     *     302 = "Returned when user is logged already",
     *     400 = "Returned when the form has errors",
     *   },
     *   requirements={
     *      {
     *          "name"="email",
     *          "dataType"="string",
     *          "requirement"="\d+",
     *          "description"="Email"
     *      },
     *      {
     *          "name"="username",
     *          "dataType"="string",
     *          "requirement"="\d+",
     *          "description"="Username"
     *      },
     *      {
     *          "name"="password",
     *          "dataType"="string",
     *          "requirement"="\d+",
     *          "description"="Password"
     *      },
     *      {
     *          "name"="repeat_password",
     *          "dataType"="string",
     *          "requirement"="\d+",
     *          "description"="Repeat password"
     *      },
     *   }
     * )
     *
     * @param Request $request
     *
     * @return View
     */
    public function postRegisterAction(Request $request)
    {
        if ($this->getUserCheck($request)) {
            $view = View::create()
              ->setStatusCode(Codes::HTTP_FOUND)
              ->setData($this->getUserParams($this->getUser()));
            return $this->get('fos_rest.view_handler')->handle($view);
        }


        $userManager = $this->container->get('fos_user.user_manager');

        $dispatcher = $this->container->get('event_dispatcher');

        $user = $userManager->createUser();

        $form = $this->container->get('form.factory')->create(new ApiRegistrationFormType(get_class($user)), $user);

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form->setData($user);

        $requestedParams = array(
          'email' => $request->request->get('email'),
          'username' => $request->request->get('username'),
          'plainPassword' => array(
            'first' => $request->request->get('password'),
            'second' => $request->request->get('repeat_password')
          )
        );

        if ('POST' === $request->getMethod()) {
            $em = $this->getDoctrine()->getManager();
            $form->bind($requestedParams);

            if ($form->isValid()) {
                $event = new FormEvent($form, $request);
                $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);
                $username = $form['username']->getData();
                $user->setUsername($username);
                $user->setCreatedAt(new \DateTime());
                $userManager->updateUser($user);

                if (null === $response = $event->getResponse()) {
                    $url = $this->container->get('router')->generate('fos_user_registration_confirmed');
                    $response = new RedirectResponse($url);
                }

                $em->flush();

                $dispatcher->dispatch(FOSUserEvents::REGISTRATION_COMPLETED, new FilterUserResponseEvent($user, $request, $response));

                $RegistrationEvent = new RegistrationEvent($user);
                $dispatcher->dispatch(AppEvents::USER_REGISTRATED, $RegistrationEvent);

                $user->setEnabled(true);
                $user->setApiToken();
                $userManager->updateUser($user);

                $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
                $this->get('security.context')->setToken($token);

                if (null === $user->getConfirmationToken()) {
                    $tokenGenerator = $this->container->get('fos_user.util.token_generator');
                    $user->setConfirmationToken($tokenGenerator->generateToken());
                    $userManager->updateUser($user);
                }

                $view = View::create()
                  ->setStatusCode(Codes::HTTP_CREATED)
                  ->setData(array($this->getUserParams($user)));
                return $this->get('fos_rest.view_handler')->handle($view);
            }
        }

        $message = 'Something went wrong.';
        $errors = $form->getErrors();

        if (count($errors) > 0) {
            $message = $errors[0];
        } else {
            $errors = $form['email']->getErrors();
            if (count($errors) > 0) {
                $message = $errors[0];
            } else {
                $errors = $form['username']->getErrors();
                if (count($errors) > 0) {
                    $message = $errors[0];
                } else {
                    $errors = $form['plainPassword']['first']->getErrors();
                    if (count($errors) > 0) {
                        $message = $errors[0];
                    } else {
                        $errors = $form['plainPassword']['second']->getErrors();
                        if (count($errors) > 0) {
                            $message = $errors[0];
                        }
                    }
                }
            }
        }

        $view = View::create()
          ->setStatusCode(Codes::HTTP_BAD_REQUEST)
          ->setData(array('message' => $message));
        return $this->get('fos_rest.view_handler')->handle($view);
    }

    /**
     * Get user data
     *
     * @ApiDoc(
     *   resource = true,
     *   section = "Users",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     403 = "Forbidden access, please login first",
     *   },
     *   requirements={
     *      {
     *          "name"="id",
     *          "dataType"="string",
     *          "requirement"="\d+",
     *          "description"="User id, if not provided logged in user will be used"
     *      },
     *   }
     * )
     *
     * @param Request $request
     *
     * @return View
     */
    public function getUsersDataAction(Request $request)
    {
        $user = $this->getUserCheck($request);

        if (!$user) {
            $view = View::create()
              ->setStatusCode(Codes::HTTP_FORBIDDEN)
              ->setData(array('message' => 'Forbidden access, please login first.'));

            return $this->get('fos_rest.view_handler')->handle($view);
        }

        $userId = $request->get('id', false);

        $userProvided = $this->getDoctrine()->getRepository('AppBundle:User')->find($userId);

        if (!$userProvided) {
            $userProvided = $user;
        }

        $data = $this->getUserParams($userProvided);

        $view = View::create()
          ->setStatusCode(Codes::HTTP_OK)
          ->setData($data);

        return $this->get('fos_rest.view_handler')->handle($view);
    }

    
    /**
     * Edit/Add student
     * If you post student id it will edit that student otherwise it will add new student.
     *
     * @ApiDoc(
     *   resource = true,
     *   section = "Users",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     403 = "Forbidden access, please login first",
     *     400 = "Returned when there is error",
     *   },
     *   requirements={
     *      {
     *          "name"="id",
     *          "dataType"="string",
     *          "requirement"="\d+",
     *          "description"="Student encrypted id(optional)"
     *      },
     *      {
     *          "name"="student[fullname]",
     *          "dataType"="string",
     *          "requirement"="\d+",
     *          "description"="Student full name(required)"
     *      },
     *      {
     *          "name"="student[birthday]",
     *          "dataType"="string",
     *          "requirement"="\d+",
     *          "description"="Student birthday (required)"
     *      }
     *   }
     * )
     * @param Request $request the request object
     *
     * @return array
     *
     */
    public function postEditStudentAction(Request $request)
    {
        $currentUser = $this->getUserCheck($request);

        if(!$currentUser || ($currentUser && !$currentUser->hasRole("ROLE_ADMIN"))){
            $view = View::create()
                ->setStatusCode(Codes::HTTP_FORBIDDEN)
                ->setData(array('message' => 'Forbidden access, only admin may manage student.'));

            return $this->get('fos_rest.view_handler')->handle($view);
        }

        $userId = $request->request->get('id', false);

        $user = $this->getDoctrine()->getRepository('AppBundle:User')->find(IDEncrypt::decrypt($userId));
        if(!$user){
            $user = new User();
            $userHelper = $this->container->get('app.user_helper');
            $userHelper->generateUserDataFromName($user->getFullName(), $user);
        }

        $studentPost = $request->request->get('student', array());

        $fullName = isset($studentPost['fullname']) ? trim($studentPost['fullname']) : '';
        if($fullName == ''){
            $view = View::create()
                ->setStatusCode(Codes::HTTP_BAD_REQUEST)
                ->setData(array('message' => 'Please enter student Full Name.'));

            return $this->get('fos_rest.view_handler')->handle($view);
        }

        if(strlen($fullName) < 3){
            $view = View::create()
                ->setStatusCode(Codes::HTTP_BAD_REQUEST)
                ->setData(array('message' => 'Student Full Name should be min 3 characters long.'));

            return $this->get('fos_rest.view_handler')->handle($view);
        }

        $birthday = isset($studentPost['birthday']) ? $studentPost['birthday'] : '';
        if($birthday == ''){
            $view = View::create()
                ->setStatusCode(Codes::HTTP_BAD_REQUEST)
                ->setData(array('message' => 'Please enter student birthday.'));

            return $this->get('fos_rest.view_handler')->handle($view);
        }

        try {
            $birthday = str_replace('/', '-', $birthday);
            $birthday = new \DateTime($birthday);
        } catch (\Exception $e) {
            $view = View::create()
                ->setStatusCode(Codes::HTTP_BAD_REQUEST)
                ->setData(array('message' => $e->getMessage()));

            return $this->get('fos_rest.view_handler')->handle($view);
        }
        
        $startDate = new \DateTime();
        $startDate->modify("-18 years");
        if($startDate < $birthday){
            $view = View::create()
                ->setStatusCode(Codes::HTTP_BAD_REQUEST)
                ->setData(array('message' => 'Student should be at least 18 years old.'));

            return $this->get('fos_rest.view_handler')->handle($view);
        }
        
        $user->setFullName($fullName)
            ->setDateOfBirth($birthday);

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        $view = View::create()
            ->setStatusCode(Codes::HTTP_OK)
            ->setData(array('message' => 'Successfully saved student.'));

        return $this->get('fos_rest.view_handler')->handle($view);
    }
    
    /**
     * Get user data
     *
     * @param User $user
     *
     * @return array
     */
    private function getUserParams(User $user)
    {
        $data = array(
          'id' => IDEncrypt::encrypt($user->getId()),
          'username' => $user->getUsername(),
          'email' => $user->getEmail(),
          'api_token' => $user->getApiToken(),
          'full_name' => $user->getFullName(),
          'date_of_birth' => $user->getDateOfBirth() ? $user->getDateOfBirth()->format('d.m.Y.') : NULL,
        );
        if (!$user->hasRole("ROLE_ADMIN") && count($user->getExamResults()) > 0) {
            foreach ($user->getExamResults() as $examResult) {
                $data['exams'][] = array(
                  'exam_name' => $examResult->getExam()->getName(),
                  'mark' => $examResult->getMark(),
                  'exam_date' => $examResult->getExamDate()->format('d.m.Y. H:i'),
                );
            }
        }
        return $data;
    }
}