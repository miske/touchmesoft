<?php

namespace AppBundle\Controller\Api;

use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;

class ApiBaseController extends FOSRestController
{

    /**
     * @param $request
     *
     * @return mixed|object
     */
    public function getUserCheck($request)
    {
        $user = $this->getUser();

        if (!$user) {
            $headers = $request->headers->all();
            if (isset($headers['authtoken'])) {
                $user = $this->getDoctrine()->getRepository('AppBundle:User')->findOneBy(array('api_token' => $headers['authtoken']));
                if ($user) {
                    $this->container->get('fos_user.security.login_manager')->loginUser(
                      $this->container->getParameter('fos_user.firewall_name'), $user
                    );
                }
            }
        }

        return $user;

    }

}