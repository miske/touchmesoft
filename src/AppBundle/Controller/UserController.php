<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * User controller.
 *
 * @Route("/user")
 */
class UserController extends Controller
{
    /**
     * Lists all User entities.
     *
     * @Route("/", name="user_index")
     * @Method({"GET", "POST"})
     * @param Request $request
     *
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $orderBy = $request->query->get('orderby', NULL);
        $order = $request->query->get('order', NULL);
        $users = $em->getRepository('AppBundle:User')->getOrderableActiveStudents($orderBy, $order);

        if ($request->isXmlHttpRequest()) {
            $html = $this->renderView('user/user_table.html.twig', array(
              'users' => $users,
              'orderBy' => $orderBy,
              'order' => $order
            ));
            $success = true;
            return new JsonResponse(array(
                'html' => $html,
                'success' => $success
              )
            );
        } else {
            return $this->render('user/index.html.twig', array(
              'users' => $users,
              'orderBy' => $orderBy,
              'order' => $order
            ));
        }
    }
}
