<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class AdminBaseController extends Controller
{

    /**
     * @return mixed
     */
    public function getUserCheck()
    {
        $user = $this->getUser();
        
        if(!$user){
            throw new AccessDeniedException();
        }
        
        if(!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
            throw new AccessDeniedException();
        }
        
        return $user;

    }

}