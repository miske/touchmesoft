<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Exam;
use AppBundle\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Exam controller.
 *
 * @Route("/exam-result")
 */
class ExamResultController extends Controller
{
    /**
     * Lists all Exam entities.
     *
     * @Route("/", name="exam_index")
     * @Method({"GET", "POST"})
     * @param Request $request
     *
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $orderBy = $request->query->get('orderby', NULL);
        $order = $request->query->get('order', NULL);
        $exams = $em->getRepository('AppBundle:ExamResult')->getOrderableExamResults($orderBy, $order);

        if ($request->isXmlHttpRequest()) {
            $html = $this->renderView('exam_result/exam_table.html.twig', array(
                'exams' => $exams,
                'orderBy' => $orderBy,
                'order' => $order
            ));
            $success = true;
            return new JsonResponse(array(
                    'html' => $html,
                    'success' => $success
                )
            );
        } else {
            return $this->render('exam_result/index.html.twig', array(
                'exams' => $exams,
                'orderBy' => $orderBy,
                'order' => $order
            ));
        }
    }

    /**
     * Lists Exam Results for given User
     *
     * @Route("/user/{username}", name="exam_result_by_user")
     * @ParamConverter("user", class="AppBundle:User", options={"mapping": {"username": "username"}})
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function userAction(Request $request, User $user)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new AccessDeniedHttpException();
        }
        $em = $this->getDoctrine()->getManager();
        $orderBy = $request->query->get('orderby', NULL);
        $order = $request->query->get('order', NULL);
        $examResulsRepo = $em->getRepository('AppBundle:ExamResult');
        $examResults = $examResulsRepo->getOrderableExamResults($orderBy, $order, $user);

        $html = $this->renderView('exam_result/result_by_user_table.html.twig', array(
            'examResults' => $examResults,
            'orderBy' => $orderBy,
            'order' => $order,
            'user' => $user
        ));
        $success = true;
        return new JsonResponse(array(
                'html' => $html,
                'success' => $success
            )
        );
    }

    /**
     * Lists Exam Results for given Exam
     *
     * @Route("/exam/{id}", name="exam_result_by_exam")
     * @ParamConverter("exam", class="AppBundle:Exam", options={"mapping": {"id": "id"}})
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function examAction(Request $request, Exam $exam)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new AccessDeniedHttpException();
        }
        $em = $this->getDoctrine()->getManager();
        $orderBy = $request->query->get('orderby', NULL);
        $order = $request->query->get('order', NULL);
        $examResulsRepo = $em->getRepository('AppBundle:ExamResult');
        $examResults = $examResulsRepo->getOrderableExamResults($orderBy, $order, null, $exam);

        $html = $this->renderView('exam_result/result_by_exam_table.html.twig', array(
            'examResults' => $examResults,
            'orderBy' => $orderBy,
            'order' => $order,
            'exam' => $exam
        ));
        $success = true;
        return new JsonResponse(array(
                'html' => $html,
                'success' => $success
            )
        );
    }
}
