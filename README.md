Touchmesoft test
============

Symfony2 Project 

### 1. Make database touchmesoft 

### 2. Clone the project with Git and run composer install:

```
php composer install
```

### 3. Run following commands to change the permissions for the following folders

```
sudo chmod 0777 -R app/cache/
sudo chmod 0777 -R app/logs/
```

### 4. Run following command to install assets

```
sh do_assets.sh
```

### 5. Doctrine migrations:

Generate a new migration for this database automatically by running the following command:

```
php app/console doctrine:migrations:diff
```

Next, run the migration to add the tables to your database:

```
php app/console doctrine:migrations:migrate
```

### 6. Load fixtures via the command line:

```
php app/console doctrine:fixtures:load --no-interaction
```


### 7. Login

**Admin:**

- Username: admin
- Password: admin

**Student:**

- Username: student1
- Password: student

**Student:**

- Username: student2
- Password: student

**Student:**

- Username: student3
- Password: student

**Student:**

- Username: student4
- Password: student

### 8. Testing API functions

If you want to test API functions you can do that in administration. Generate url for that purpose as below:

```
localhost/api/doc
```

### 9. Creating Exam Results

Only admin can create/edit students, and exam result.

When managing (create/edit) exam result enter exam name in appropriate field. If exam exist it will appear in autocomplete list, if not exist it will be 
created once you submit form. 

Exam date must be in the past. You can choose date and time when the exam was held.

There is no restriction on the number of times User can re-sit the exams. That's not requested ;)

Order is maintained when adding, editing and deleting.

### 10. Creating Student

Birthday - student should be at least 18 years old. Also it can be too old, in case you want to enter the data for the Faculty since establishment.

Order is maintained when adding, editing and deleting.

### 11. Manage student and Exams

Back-end part should be available only with admin login. 
All data management is available in FE and BE, but only with admin login. It can be done without and with page reload.

